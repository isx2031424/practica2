Tasca 6:
1) Identifiqueu quin són els fitxers (ruta absoluta) de definició system wide.

/etc/profile.d
/etc/profile
/etc/bashrc

2) Identifiqueu quins són els fitxers de personalització individual d’usuari.

.bash_profile  
.bashrc 

3) Identifiqueu a quins fitxers es defineixen les variables d’entorn i els startup programs.

/etc/profile
.bash_profile

4) Identifiqueu a quins fitxers es defineixen les funcions i els àlies.

/etc/bashrc
.bashrc

respectivament per sytem wide i de l'usuari

Tasca 7:
1) Com a administrador defineix 3 variables d’entorn i 3 àlies.

per definir l'alias hem d'editar el /etc/bashrc:
alias yo='id;whoami'
alias fecha='date;cal'
alias tiempo='uptime -p'

i se suposa que les variables d'entorn s'ha de editar el fitxer /etc/profile pero a mi no em funciona,
en canvi quan poso les variables d'entorn a bashrc tambe si que em funciona.

alias yo='whoami'
alias espacio='df -h'
alias uso='du -sh'
nom=$USER
CAMI=$PATH

2) Com a usuari defineix una variable d’entorn i un àlies propi.

Em passa el mateix que al system wide, he de posar tant els alias com les variables d'entorn al fitxer .bashrc

alias date='cal'
cognom=sanchez


3) Com a usuari redefineix una variable d’entorn i un àlies dels que havia definit 
l’administrador.

El que passa és que pren prioritat les variables canviades per l'usuari.
