#! /bin/bash 
#Diego sanchez
#asix m01
#programa que crea un grup d'usuaris donat el nom del curs

ERR_NARGS=1
OK=0
#comprovar que es rep un arg
if [ $# -ne 1 ]; then
  echo "error: nº d'args $# incorrecte" >> /dev/stderr
  echo "usage: $0 curs" >> /dev/stderr
  exit 1
fi

#xixa
curs=$1
mkdir /home/$curs
userlist=$(echo $curs-{1..30})

for user in $userlist; do
  useradd -g $curs -G users -m -b /home/$curs$user
  echo "$user:alum" | chpasswd
done
exit $OK
